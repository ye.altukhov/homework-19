run docker-compose up -d

create table without partitions:

CREATE TABLE employees (
id bigint not null,
name character varying not null,
position character varying not null,
job_id  int not null,
start_year int not null )

Patitioned by shards:

create table on main shard:

CREATE TABLE employees (
id bigint not null,
name character varying not null,
position character varying not null,
job_id  int not null,
start_year int not null )

create table on shard1:

CREATE TABLE employees (
id bigint not null,
name character varying not null,
position character varying not null,
job_id  int not null,
start_year int not null,
CONSTRAINT job_id_check CHECK ( job_id = 1 )
);

create table on shard2:

CREATE TABLE employees (
id bigint not null,
name character varying not null,
position character varying not null,
job_id  int not null,
start_year int not null,
CONSTRAINT job_id_check CHECK ( job_id = 2 )
);

create index on table shard field
CREATE INDEX employees_job_id_idx ON employees USING btree(job_id);

create foreign data wrapper:
CREATE EXTENSION postgres_fdw;
CREATE SERVER shard_1 FOREIGN DATA WRAPPER postgres_fdw OPTIONS( host 'slave1', port '5432', dbname 'homework19' );
CREATE SERVER shard_2 FOREIGN DATA WRAPPER postgres_fdw OPTIONS( host 'slave2', port '5432', dbname 'homework19' );
CREATE USER MAPPING FOR postgres SERVER shard_1 OPTIONS (user 'admin', password '12345');
CREATE USER MAPPING FOR postgres SERVER shard_2 OPTIONS (user 'admin', password '12345');

create foreign tables on main:

CREATE FOREIGN TABLE employees_shard1 (
id bigint not null,
name character varying not null,
position character varying not null,
job_id  int not null,
start_year int not null )
SERVER shard_1
OPTIONS (schema_name 'public', table_name 'employees');

CREATE FOREIGN TABLE employees_shard2 (
id bigint not null,
name character varying not null,
position character varying not null,
job_id  int not null,
start_year int not null )
SERVER shard_2
OPTIONS (schema_name 'public', table_name 'employees');

perfomance:

python init.py

123.73220304011156
123.83990301111521
123.44839510472312
123.99434051231234
124.00193232501275